@rem showcase.cmd

@rem This script runs the unit test with code coverage enabled, then 
@rem runs the script to analyze a couple of remote repositories and
@rem finally runs pylint.
@rem It is intended as a general sanity test.

@echo off
cls

set VENV_DIR=venv3
set VENV_PYTHON=%VENV_DIR%\Scripts\python.exe
set VENV_COVERAGE=%VENV_DIR%\Scripts\coverage.exe

rem rmdir /s/q %VENV_DIR%
if not exist "%VENV_DIR%" (
	py -3 -m venv %VENV_DIR%
	%VENV_PYTHON% -m pip install --upgrade pip
	%VENV_PYTHON% -m pip install pylint 
	%VENV_PYTHON% -m pip install coverage==3.7.1
)

%VENV_COVERAGE% erase
%VENV_COVERAGE% run --source=vceffort showcase.py run_unit_tests
%VENV_COVERAGE% report

%VENV_COVERAGE% erase
%VENV_COVERAGE% run --source=vceffort showcase.py run_unit_and_integration_tests
%VENV_COVERAGE% report

%VENV_PYTHON% showcase.py run_samples
%VENV_PYTHON% showcase.py run_pylint
