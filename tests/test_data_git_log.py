#! python3
# encoding: utf-8

# Copyright Tim Littlefair 2016
# This work is licensed under the MIT License.
# https://opensource.org/licenses/MIT

# This file exports a single constant
# which is a long character string generated
# by running the command 'git log --date=iso'
# on the project's own repository

TEST_DATA_GIT_LOG = """
commit 35536c499b51b35445527ff6769f1245576d210a
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-11 11:22:00 +0800

    Fixed command line invocation and usage message.

commit 9a60c00866fa36a33f5f6bbb79028d431687be59
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-11 11:00:06 +0800

    Tested extractor on some sample data.
    
    Coverage still at 100%
    PyLint reporting missing docstrings and two other warnings.

commit 1aff2ac65d6f3afd44884ace48dd253eb477af74
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-11 09:31:24 +0800

    Unit testing of Timesheet now asserts on content of report.

commit 6e13ead56610e436750a457a67af264459462ed7
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-10 23:59:52 +0800

    Added report

commit 05c7e75a3c0ea3be82d90d55090569e7720b43b0
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-10 23:28:33 +0800

    Added support for between assumption parameter.

commit e02c6ad0ff163bfa0d50f316300db263d80e3fff
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-10 21:57:37 +0800

    Coverage at 100%

commit 6adf0ec3adcbd8bad6eb3bd587c2e22da5ea0d22
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-10 18:52:04 +0800

    Pylint clean apart from missing docstrings.

commit 496db2962f49428d98a02f08b5512328eaf4d411
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-10 18:05:41 +0800

    Implemented basic unit test of the WorkDayEntry class.

commit 520c2ed30fa04a3710a0ccdc7767edc2f8a43e6a
Author: Tim Littlefair <tim.littlefair@dont.spam.me.com>
Date:   2016-09-10 17:36:28 +0800

    Basic eclipse project.
    Utility now produces some output.

"""