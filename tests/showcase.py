#! python3
# encoding: utf-8

# Copyright Tim Littlefair 2016
# This work is licensed under the MIT License.
# https://opensource.org/licenses/MIT

# The next 3 lines are modelled on the context.py file described
# on Kenneth Reitz's 'Structuring your project' page:
# http://docs.python-guide.org/en/latest/writing/structure/
# Under Python 3, adding a relative directory to the path doesn't work 
# if the path manipulation is in a module which is imported.
# http://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

import unittest 
import vceffort
import test_vceffort
import pylint.lint

def print_separator():
	print('#'*75)
	sys.stdout.flush()
	
def run_unit_tests():
	print_separator()
	print("Runninng unit tests only")
	suite = unittest.TestLoader().loadTestsFromTestCase(test_vceffort.VCEffortUnitTests)
	result = unittest.result.TestResult()
	suite.run(result)
	print('')
	print("Unit test status: ",result.__repr__())
	print_separator()
	sys.stdout.flush()

def run_unit_and_integration_tests():
	print_separator()
	print("Runninng unit and integration tests")
	suite = unittest.TestLoader().loadTestsFromModule(test_vceffort)
	result = unittest.result.TestResult()
	suite.run(result)
	print('')
	print("All test status: ",result.__repr__())
	print_separator()
	sys.stdout.flush()

def run_samples():
	print_separator()
	print("Running vceffort against sample repositories")
	## Sample CLI runs
	
	# One of the inspirations for this project is the Open Source
	# Software Quality contest run by Yegor Bugayenko.
	# http://www.yegor256.com/2015/04/16/award.html
	# The repository URL below is for the 2015 winner.
	_REPO_URL_1 = "https://github.com/Suseika/inflectible.git"
	vceffort.command_line_interface((_REPO_URL_1,), sys.stdout)
	
	print_separator()
	
	# My own CCCC repository (subversion example)
	_REPO_URL_2 = "http://svn.code.sf.net/p/cccc/code"
	vceffort.command_line_interface(('--svn', _REPO_URL_2,), sys.stdout)
	print_separator()

def run_pylint():
	print_separator()
	print("Analysing vceffort with pylint")
	## PyLint report
	# docstrings will be added under this future product backlog item:
	# https://bitbucket.org/tim_littlefair/vceffort/issues/4/resolve-pylint-missing-docstring-messages
	# Until this is done, message C0111 is disabled to enable a clean Jenkins run. 
	pylint.lint.Run(['vceffort',])
	sys.exit(0)	
	print_separator()

if __name__ == "__main__":
	for operation in sys.argv[1:]:
		if(operation=="run_unit_tests"): 
			run_unit_tests()
		elif(operation=="run_unit_and_integration_tests"): 
			run_unit_and_integration_tests()
		if(operation=="run_samples"): 
			run_samples()
		if(operation=="run_pylint"): 
			run_pylint()
