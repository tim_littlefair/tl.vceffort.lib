#! python3
# encoding: utf-8

# Copyright Tim Littlefair 2016
# This work is licensed under the MIT License.
# https://opensource.org/licenses/MIT

# The next 3 lines are modelled on the context.py file described
# on Kenneth Reitz's 'Structuring your project' page:
# http://docs.python-guide.org/en/latest/writing/structure/
# Under Python 3, adding a relative directory to the path doesn't work 
# if the path manipulation is in a module which is imported.
# http://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

import datetime
import io
import tempfile
import unittest
import urllib.parse
import urllib.request

from unittest.case import skipIf

from vceffort import Assumptions, WorkDayEntry, WorkLog
from vceffort import GitLogExtractor, SvnLogExtractor, args_to_extractors, command_line_interface
from vceffort import workaround_python_issue26660

has_svn=True
has_git=True

global only_run_unit_tests 

class VCEffortUnitTests(unittest.TestCase):
    def test_wde(self):
        # Create a WDE with two events on the same day separated by 24 minutes
        first_event_time = datetime.datetime.now()
        tz8=datetime.timezone(datetime.timedelta(hours=8))
        first_event_time=first_event_time.replace(hour=9,minute=0,second=0,microsecond=0)
        first_event_time=first_event_time.replace(tzinfo=tz8)
        second_event_time = first_event_time + datetime.timedelta(0.4/24.0)
        third_event_time = first_event_time + datetime.timedelta(1.7/24.0)
        wde = WorkDayEntry("Me",first_event_time)
        wde.add_event(second_event_time)
        wde.add_event(third_event_time)

        # Default assumptions will be:
        # + work started 0.75 hours before first event in group
        # + events on the same day separated by up to 2.00 hours
        #   should be treated as being in the same group
        # + work ended 0.25 hours after last event in group
        # Under these assumptions the three events are treated as
        # a single group, and the total work time before rounding
        # is 2.7 hours
        self.assertEqual(2.70,wde.work_hours(Assumptions(0.75,2.00,0.25),0.10))
        self.assertEqual(2.75,wde.work_hours(Assumptions(0.75,2.00,0.25),0.25))
        self.assertEqual(3.00,wde.work_hours(Assumptions(0.75,2.00,0.25),1.00))

        # Effect of different assumptions:

        # As for default assumptions except:
        # + events on the same day separated by up to 1.00 hours
        #   should be treated as being in the same group
        # Under these assumptions the three events are treated as
        # two groups, the first contributing 1.4 hours, the second
        # contributing 1.0 hours
        self.assertEqual(2.40,wde.work_hours(Assumptions(0.75,1.00,0.25),0.10))

        # + work started 0.1 hours before first event in group
        # + events on the same day separated by up to 0.0 hours
        #   should be treated as being in the same group
        # + work ended 0.1 hours after last event in group
        # + events on the same day separated by up to 2.00 hours
        #   should be treated as being in the same group
        # Under these assumptions the three events are treated as
        # three separate groups, and the total work time before rounding
        # is 0.6 hours
        self.assertEqual(0.6,wde.work_hours(Assumptions(0.10,0.00,0.10),0.1))

        # + work started 0.2 hours before first event in group
        # + events on the same day separated by up to 0.0 hours
        #   should be treated as being in the same group
        # + work ended 0.3 hours after last event in group
        # + events on the same day separated by up to 2.00 hours
        #   should be treated as being in the same group
        # Under these assumptions the three events are treated as
        # two groups, groups, the first contributing 0.9 hours
        # and the total work time before rounding, the second
        # contributing 0.5 hours.  The total time before rounding
        # is 1.4 hours
        self.assertEqual(1.4,wde.work_hours(Assumptions(0.20,0.00,0.30),0.1))
        # Check the key function.
        self.assertEqual("Me",wde.key()[2])
        # Check the assertions
        try :
            event_time_on_wrong_day=first_event_time+datetime.timedelta(1.0)
            self.assertRaises(RuntimeError,wde.add_event(event_time_on_wrong_day))
        except RuntimeError:
            pass
        try :
            tz12=datetime.timezone(datetime.timedelta(hours=12))
            event_time_in_wrong_timezone=first_event_time.astimezone(tz12)
            self.assertRaises(RuntimeError,wde.add_event(event_time_in_wrong_timezone))
        except RuntimeError:
            pass
        try :
            self.assertRaises(RuntimeError,wde.work_hours(Assumptions(0.75,2.00,0.25),0.15))
        except RuntimeError:
            pass

    def test_work_log(self):
        # Create a WDE with two events on the same day separated by 24 minutes
        first_event_time = datetime.datetime.now()
        tz8=datetime.timezone(datetime.timedelta(hours=8))
        first_event_time=first_event_time.replace(hour=9,minute=0,second=0,microsecond=0)
        first_event_time=first_event_time.replace(tzinfo=tz8)
        second_event_time = first_event_time + datetime.timedelta(0.4/24.0)
        third_event_time = first_event_time + datetime.timedelta(1.7/24.0)
        work_log = WorkLog()
        work_log.add_event("Bill",first_event_time)
        work_log.add_event("Fred",second_event_time)
        work_log.add_event("Bill",third_event_time)
        report_stream = io.StringIO()
        work_log.report(report_stream)
        self.assertIn("Total hours worked: 3.7",report_stream.getvalue())

    def test_git_extractor(self):
        work_log = WorkLog()
        from test_data_git_log import TEST_DATA_GIT_LOG
        git_log_stream = io.StringIO(TEST_DATA_GIT_LOG)
        extractor = GitLogExtractor(None,stream=git_log_stream)
        extractor.feed(work_log)
        report_stream = io.StringIO()
        work_log.report(report_stream)
        self.assertIn("Total active contributor days: 2",report_stream.getvalue())

    def test_svn_extractor(self):
        work_log = WorkLog()
        from test_data_svn_log import TEST_DATA_SVN_LOG
        svn_log_stream = io.StringIO(TEST_DATA_SVN_LOG)
        extractor = SvnLogExtractor(None,stream=svn_log_stream)
        extractor.feed(work_log)
        report_stream = io.StringIO()
        work_log.report(report_stream)
        self.assertIn("Total active contributor days: 59",report_stream.getvalue())

    def test_command_line_interface_error_nonsense_arg(self):
        with self.assertRaises(RuntimeError):
            with open(os.devnull,'w') as ostream:
                command_line_interface(('[]()\/',), ostream)

    # End of unit tests

# All tests beyond this point are integration tests
# which require filesystem or network access or both.
# They are grouped into a separate unit test class 
class VCEffortUnitAndIntegrationTests(unittest.TestCase):

    def test_args_to_extractors_local_git(self):
        extractors = args_to_extractors(('../.git',))
        self.assertEqual(1,len(extractors))
        self.assertTrue(isinstance(extractors[0],GitLogExtractor))
        extractors = args_to_extractors(("--git", "../."))
        self.assertEqual(1,len(extractors))
        self.assertTrue(isinstance(extractors[0],GitLogExtractor))

    def test_args_to_extractors_local_svn(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            repo_dir = os.path.join(temp_dir,"repo")
            sandbox_dir = os.path.join(temp_dir,"sandbox")
            os.system('svnadmin create %s' % (repo_dir,))
            canonical_svn_repo_url = urllib.parse.urljoin(
                "file:", urllib.request.pathname2url(repo_dir)
            )
            # print(repo_dir, canonical_svn_repo_url)
            os.system('svn checkout %s %s > %s 2>&1' % (canonical_svn_repo_url,sandbox_dir,os.devnull))
            extractors = args_to_extractors((sandbox_dir,))
            self.assertEqual(1,len(extractors))
            self.assertTrue(isinstance(extractors[0],SvnLogExtractor))
            workaround_python_issue26660(temp_dir)

    def test_args_to_extractors_no_vc(self):
        with self.assertRaises(RuntimeError):
            with tempfile.TemporaryDirectory() as temp_dir:
                args_to_extractors((temp_dir,))

    def test_command_line_interface_nominal_remote_git(self):
        with open(os.devnull,'w') as ostream:
            command_line_interface(("https://bitbucket.org/tim_littlefair/vceffort.git",),ostream)

    def test_local_git(self):
        _TARGET_PATH = "../.git"
        work_log = WorkLog()
        extractor = GitLogExtractor(_TARGET_PATH)
        extractor.feed(work_log)
        report_stream = io.StringIO()
        work_log.report(report_stream)
        self.assertIn("Total active contributor days:",report_stream.getvalue())

    def test_command_line_interface_nominal_remote_svn(self):
        with open(os.devnull,'w') as ostream:
            command_line_interface(("--svn", "http://svn.code.sf.net/p/cccc/code",),ostream)          
 
    def test_remote_git(self):
        _TARGET_URL = "https://bitbucket.org/tim_littlefair/vceffort.git"
        work_log = WorkLog()
        extractor = GitLogExtractor(_TARGET_URL)
        extractor.feed(work_log)
        report_stream = io.StringIO()
        work_log.report(report_stream)
        self.assertIn("Total active contributor days:",report_stream.getvalue())

    def test_remote_svn(self):
        _TARGET_URL = "http://svn.code.sf.net/p/cccc/code"
        work_log = WorkLog()
        extractor = SvnLogExtractor(_TARGET_URL)
        extractor.feed(work_log)
        report_stream = io.StringIO()
        work_log.report(report_stream)
        self.assertIn("Total active contributor days:",report_stream.getvalue())

    def test_command_line_interface_error_bad_url(self):
        with self.assertRaises(RuntimeError):
            with open(os.devnull,'w') as ostream:
                command_line_interface(('https://nobody@bitbucket.org/time_litlefair/vceffort.git',), ostream)

if __name__ == "__main__": # pragma: no cover
    unittest.main()
