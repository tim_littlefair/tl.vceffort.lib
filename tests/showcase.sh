#!/bin/sh

# This script runs pylint, then runs the unit test with code coverage
# enabled, then runs the analysis on the script's own repository.
# It is intended as a general sanity test.

VENV_DIR=venv3
VENV_PYTHON=$VENV_DIR/bin/python
VENV_COVERAGE=$VENV_DIR/bin/coverage

# rm -rf venv3
if [ ! -d $VENV_DIR ]
then
	python3 -m venv $VENV_DIR
	$VENV_PYTHON -m pip install --upgrade pip
	$VENV_PYTHON -m pip install pylint 
	$VENV_PYTHON -m pip install coverage==3.7.1
fi

$VENV_COVERAGE erase
$VENV_COVERAGE run --source=vceffort showcase.py run_unit_tests
$VENV_COVERAGE report

$VENV_COVERAGE erase
$VENV_COVERAGE run --source=vceffort showcase.py run_unit_and_integration_tests
$VENV_COVERAGE report

$VENV_PYTHON showcase.py run_samples
$VENV_PYTHON showcase.py run_pylint
